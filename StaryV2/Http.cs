using System;
using System.Net;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using System.Collections.Generic;
using Meebey.SmartIrc4net;

namespace StaryV2
{
	public static class Http
	{
		public static Dictionary<string,Bot> Bots = new Dictionary<string, Bot>();
		
		static HttpListener listener = new HttpListener();
		
		static void Serve()
		{
			while(true)
			{
				HttpListenerContext ctx = listener.GetContext();
				HttpListenerRequest req = ctx.Request;
				ctx.Response.StatusCode = 200;
				string json="";
				
				if(ctx.Request.RawUrl == "/")
				{
					json = JsonConvert.SerializeObject(new { Version = 1, Bots=Bots.Keys });
				}
				else if(ctx.Request.RawUrl.EndsWith("/users"))
				{
					string bot = ctx.Request.Url.AbsolutePath.Split(new char[1]{'/'},StringSplitOptions.RemoveEmptyEntries)[0];
					
					Dictionary<string,List<string>> channels = new Dictionary<string,List<string>>();
					foreach(string chan in Bots[bot].Irc.GetChannels())
					{
						channels[chan]=new List<string>();
						Channel c = Bots[bot].Irc.GetChannel(chan);
						foreach(string user in c.Users.Keys)
						{
							channels[chan].Add(user);
						}
					}
					json = JsonConvert.SerializeObject(new { Bot=bot, Channels = channels });
				}
				ctx.Response.ContentLength64 = json.Length;
				StreamWriter w = new StreamWriter(ctx.Response.OutputStream);
				w.Write(json);
				w.Close();
			}
		}
		
		public static void Init()
		{
			listener.Prefixes.Add("http://*:9999/");
			listener.Start();
			Thread thread = new Thread(Serve);
			thread.Start();
		}
	}
}

