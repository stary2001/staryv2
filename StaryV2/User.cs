using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace StaryV2
{
	public partial class Commands
	{
		public static void Register(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length != 0 && Configuration.Get("users." + args[0]) != null)
			{
				bot.Irc.RfcPrivmsg(channel, "Account already exists!");
				return;
			}

			if(args.Length >= 1 + (bot.UseNickserv ? 0 : 1)) // require password if no NickServ
			{
				if(bot.UseNickserv)
				{
					string ns = bot.GetNickserv(nick);
					Configuration.Set("users." + args[0], new User(args[0],bot.NetworkName, ns,args.Length >= 2 ? User.SHA512Hex(args[1]) : null));
					bot.Irc.RfcPrivmsg(channel, "User created.");
				}
				else
				{
					Configuration.Set("users." + args[0], new User(args[0],bot.NetworkName, null,User.SHA512Hex(args[1])));
					bot.Irc.RfcPrivmsg(channel, "User created.");
				}
			}
			else if(args.Length == 1 && !bot.UseNickserv)
			{
				bot.Irc.RfcPrivmsg(channel, "Network has no services, and you did not supply a password.");
			}
			else
			{
				bot.Irc.RfcPrivmsg(channel, "Usage: <account name> [password]");
			}
		}

		public static void Login(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length != 0)
			{
				User user = User.GetByUsername(args[0]);
				if(user == null)
				{
					bot.Irc.RfcPrivmsg(channel,"There is no user called " + args[0]);
					return;
				}

				if(user.Login(bot,nick,args))
				{
					bot.Irc.RfcPrivmsg(channel, "Authentication successful.");
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel,"Authentication failed.");
				}
			}
			else
			{
				bot.Irc.RfcPrivmsg(channel,"Usage: <username> [password]");
			}
		}

		public static void Logout(Bot bot, string nick,string channel, string[] args)
		{
			User user = User.GetOnNetwork(bot, nick ,bot.NetworkName);
			if(user != null)
			{
				user.Logout(bot, nick);
			}
		}

		public static void Password(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length != 0)
			{
				User user = User.GetOnNetwork(bot, nick ,bot.NetworkName);
            	if(user != null)
            	{
                	user.Password=User.SHA512Hex(args[0]);
            	}
			}
		}
	}

	class User
	{
		public string Username;
		Dictionary<string,string> NickservAccounts;
		public string Password;

		public User()
		{}

		public User(string username,string network, string nickservAccount = null, string password=null)
		{
			Username = username;
			NickservAccounts = new Dictionary<string,string>();
			if(nickservAccount != null)
			{
				NickservAccounts[network] = nickservAccount;
			}
			Password = password;
		}

		public static User GetByUsername(string username)
		{
			return Configuration.Get<User>("users." + username);
		}

		public static User GetOnNetwork(Bot bot, string nick, string network)
		{
			if(nicks.ContainsKey(network) && nicks[network].ContainsKey(nick))	
			{
				return GetByUsername(nicks[network][nick]);
			}
			else
			{
				return null;
			}
		}

		public bool Login(Bot bot, string nick,string[] args)
		{
			string network = bot.NetworkName;
			string ns = bot.GetNickserv(nick);

			if(bot.UseNickserv)
			{
				if(NickservAccounts.ContainsKey(network) && ns == NickservAccounts[network])
				{
					if(!nicks.ContainsKey(network))
					{
						nicks[network] = new Dictionary<string,string>();
					}
					if(!nickservs.ContainsKey(network))
					{
						nickservs[network] = new Dictionary<string,string>();
					}
					nicks[network][nick] = this.Username;
					nickservs[network][ns] = this.Username;
					return true;
				}
			}

			if(args.Length >= 2 && args[1] == SHA512Hex(Password))
			{
				if(!nicks.ContainsKey(network))
				{
					nicks[network] = new Dictionary<string,string>();
				}

				nicks[network][nick] = this.Username;
				if(bot.UseNickserv)
				{
					if(!nickservs.ContainsKey(network))
					{
						nickservs[network] = new Dictionary<string,string>();
					}
					nickservs[network][ns] = this.Username;
				}
				return true;
			}
			return false;
		}
		
		public void Logout(Bot bot, string nick)
		{
			string network = bot.NetworkName;
			string ns = bot.GetNickserv(nick);
			
			nicks[network].Remove(nick);
			nickservs[network].Remove(ns);
		}
		
		public static string SHA512Hex(string input)
		{
			byte[] inputBytes = Encoding.UTF8.GetBytes(input);
			var alg = SHA512.Create();
			byte[] outputBytes= alg.ComputeHash(inputBytes);
			StringBuilder outputBuilder = new StringBuilder();
			foreach (byte b in outputBytes)
			{
				outputBuilder.Append(String.Format("{0:x2}", b));
			}
			return outputBuilder.ToString();
		}

		public static Dictionary<string,Dictionary<string,string>> nicks = new Dictionary<string,Dictionary<string,string>>();
		public static Dictionary<string,Dictionary<string,string>> nickservs = new Dictionary<string,Dictionary<string,string>>();
	}
}
