using System;
using System.IO;

using System.Collections.Generic;
using System.Linq;
using Meebey.SmartIrc4net;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using Newtonsoft.Json;

namespace StaryV2
{
	public delegate void Command(Bot bot, string nick,string channel, string[] args);

	public class CommandException : Exception
	{
		public CommandException(string msg) : base(msg) {}
	}
	
	public class Bot
	{
		public string NetworkName;
		
		public static string OurHost;
		public Bot(string networkName)
		{
			MainClass.NumBots++;
			NetworkName = networkName;
			DefaultPrefixes.Add("@");
			DefaultPermissions.Add("Stary2001");
			Thread t = new Thread (Commands.TimerThread);
			t.Start ();

			Irc = new IrcClient ();

			Irc.ActiveChannelSyncing = true;
			Irc.SupportNonRfc = true;
			
			Irc.AutoRejoinOnKick = true;
			Irc.AutoJoinOnInvite = true;
			Irc.AutoNickHandling = true;
			Irc.AutoReconnect = true;
			Irc.SupportNonRfc = true;
			
			Irc.OnChannelMessage += OnChannelMessage;
			Irc.OnQueryMessage += OnChannelMessage;
			
			Irc.OnNickChange += OnNickChange;
			
			foreach(MethodInfo mi in typeof(Commands).GetMethods ())
			{
				ParameterInfo[] pinfo = mi.GetParameters();
				if(pinfo.Length == 4 &&
				   pinfo[0].ParameterType == typeof(Bot) &&
				   pinfo[1].ParameterType == typeof(string) &&
				   pinfo[2].ParameterType == typeof(string) &&
				   pinfo[3].ParameterType == typeof(string[]) && mi.IsStatic)
				{
					//Console.WriteLine ("Adding command '" + mi.Name.ToLower() + "'");
					CommandList[mi.Name.ToLower()] = (Command) Delegate.CreateDelegate(typeof(Command),mi);
				}
			}
		}
	
		public void Connect(string nick=null)
		{
			UseNickserv = Configuration.Get<bool>(NetworkName + ".irc.useNickserv");
			bool ssl = Configuration.Get<bool>(NetworkName + ".irc.ssl");

			Irc.UseSsl = ssl;
			Irc.ValidateServerCertificate = false;

			string server = Configuration.Get<string>(NetworkName + ".irc.server");
			string user = Configuration.GetOrCreate<string>(NetworkName + ".irc.username",string.Empty);
			string pass = Configuration.GetOrCreate<string>(NetworkName + ".irc.password",string.Empty);
			
			long longPort = Configuration.Get<long>(NetworkName + ".irc.port");
			int port = Convert.ToInt32(longPort);
			
			if(nick==null)
			{
				nick = Configuration.Get<string>(NetworkName + ".irc.nick");
			}
			
			Console.WriteLine ("Connecting to server '" + server + ":" + port.ToString() +  "' with nick '" + nick + "'");
			
			Irc.Connect(server, port);
			if(user == "")
			{
				Irc.Login(nick, "A bot.",0);
			}
			else
			{
				Irc.Login(nick, "A bot.",0,user,pass);
			}

			if(Configuration.Get(NetworkName + ".irc.nickservUser") != null)
			{
				Irc.RfcPrivmsg("NickServ", "identify " + Configuration.Get<string>(NetworkName + ".irc.nickservUser") + " " + Configuration.Get<string>(NetworkName + ".irc.nickservPass"));
			}
			
			foreach (string s in Configuration.GetOrCreate<List<string>>(NetworkName + ".irc.autoJoinList",null))
			{
				Console.WriteLine ("Joining channel '" + s + "'");
				Irc.RfcJoin (s);
			}
			//Irc.OnNickChange += (object sender, NickChangeEventArgs e) =>  { Console.WriteLine("{0}, {1}", e.OldNickname,e.NewNickname); };
		}

		public void GenConf(string net)
		{
			Console.WriteLine(net);
			if(Configuration.GetNode(net + ".irc") == null)
			{
				Console.WriteLine("No config found for network " + net);

				Console.Write("irc.server: ");
				Configuration.Set(net + ".irc.server",Console.ReadLine());
				Console.Write("irc.port: ");
				Configuration.Set(net + ".irc.port",long.Parse(Console.ReadLine()));

				Console.Write("irc.ssl: ");
				Configuration.Set(net + ".irc.ssl",bool.Parse(Console.ReadLine()));


				Console.Write("irc.username (enter skips): ");
				string user = Console.ReadLine();
				if(user != "")
				{
					Configuration.Set(net + ".irc.username",user);
					Console.Write("irc.password: ");
					Configuration.Set(net + ".irc.password",Console.ReadLine());
				}

				Console.Write("irc.nick: ");
				Configuration.Set(net + ".irc.nick",Console.ReadLine());
				Console.Write("irc.nickservUser (enter skips): ");
				user = Console.ReadLine();
				if(user != "")
				{
					Configuration.Set(net + ".irc.nickservUser",user);
					Console.Write("irc.nickservPass: ");
					Configuration.Set(net + ".irc.nickservPass",Console.ReadLine());
				}
				Console.Write("Does the server have services? ((y)es/(n)o) ");
				string s = Console.ReadLine();
				if(s != "y" && s != "n" && s != "yes" && s != "no")
				{
					Console.WriteLine("Please enter (y)es or (n)o.");

					while(s!="y" && s != "n" && s != "yes" && s != "no")
					{
						s = Console.ReadLine();
					}
				}

				if(s == "no" || s == "n")
				{
					Configuration.Set(net + ".irc.useNickserv",false);
				}
				else
				{
					Configuration.Set(net + ".irc.useNickserv",true);
				}

				Configuration.Set(net + ".irc.autoJoinList",new List<string>());

				Console.WriteLine("All good!");

				Configuration.Save();
			}
		}

		public void Listen()
		{
			Irc.Listen();
		}

		public Dictionary<string,List<string>> lastMessages = new Dictionary<string, List<string>>();
		
		List<string> DefaultPermissions = new List<string>();
		List<string> DefaultPrefixes = new List<string>();
		
		public void OnChannelMessage(object sender,IrcEventArgs e)
		{
			string ns = GetNickserv (e.Data.Nick);
			
			if(ns != null)
			{
				if(!lastMessages.ContainsKey(ns))
				{
					lastMessages[ns] = new List<string>();
				}
			
				if(lastMessages[ns].Count == 50)
				{
					lastMessages[ns].RemoveAt(0);
				}
				lastMessages[ns].Add(e.Data.Message);
			}
			
			var prefixes = Configuration.GetOrCreate(NetworkName + ".irc.prefixes",DefaultPrefixes);
			
			string thePrefix = null;
			foreach(string prefix in prefixes)
			{
				if (e.Data.Message.StartsWith (prefix) || (e.Data.Message.StartsWith (prefix) && thePrefix.Length < prefix.Length))
				{
					thePrefix = prefix;
				}
			}	
			
			if(thePrefix != null)
			{
				string message = e.Data.Message.Substring (thePrefix.Length);

				string[] messageArray = message.Split (' ');
				
				OnCommand (e.Data.Nick, e.Data.Host, e.Data.Channel ?? e.Data.Nick, message,messageArray);
			}
		}

		void Run(string cmd, string nick, string channel, string[] messageArray)
		{
			try
			{
				CommandList [cmd] (this, nick, channel, messageArray);
			} 
			catch(CommandException e)
			{
				Irc.RfcPrivmsg(channel, e.Message);
			}
			catch (Exception e)
			{
				Irc.RfcNotice (nick, "Command threw an exception : '" + e.Message + "'!");
				Console.WriteLine(e.Message + " " + e.StackTrace);
			}
		}
		
		public void OnCommand(string nick,string host, string channel, string message , string[] messageArray)
		{
			string cmd = messageArray[0];
			if (CommandList.ContainsKey (cmd))
			{
				User user = User.GetOnNetwork(this, nick, NetworkName);
					
				bool global = false;
				List<string> perms = Configuration.GetOrCreate<List<string>>("perms.cmd." + cmd, DefaultPermissions);

				foreach (string s in perms)
				{
					if ((user != null ? s == user.Username : false) || s == "*")
					{
						global = true;
						break;
					}
				}

				if (global)
				{
					Run(cmd, nick, channel, messageArray.Skip (1).ToArray ());
				}
				else
				{
					bool network = false;
					perms = Configuration.GetOrCreate<List<string>>(NetworkName + ".perms.cmd." + cmd, DefaultPermissions);

					foreach (string s in perms)
					{
						if ((user != null ? s == user.Username : false) || s == "*")
						{
							network = true;
							break;
						}
					}

					if(network)
					{
						Run(cmd, nick, channel, messageArray.Skip (1).ToArray ());
					}
					else
					{
						bool local = false;
						perms = Configuration.GetOrCreate<List<string>>(NetworkName + ".channel." + channel + ".perms.cmd." + cmd,  DefaultPermissions);
						foreach (string s in perms)
						{
							if ((user != null ? s == user.Username : false) || s == "*")
							{
								local = true;
							}
						}

						if(local)
						{
							Run(cmd, nick, channel, messageArray.Skip (1).ToArray ());
						}
						else if(user == null)
						{
							Irc.RfcPrivmsg(channel, "You are not logged in. Use @login to login. @register will make an account.");
							return;
						}
						else
						{
							Irc.RfcNotice (nick, "You are missing permissions. Bai.");
						}
					}
				}
			} 
			else if (Configuration.Get<Dictionary<string,Factoid>>("factoid.data").ContainsKey (messageArray [0]))
			{
				var factoids = (Dictionary<string,Factoid>)Configuration.Get ("factoid.data");
				string[] fArgs = null;
 				if(factoids[messageArray[0]].ArgStyle == "single")
				{
					int p1 = message.IndexOf(" ");
					if(p1 == -1) p1 = 0;
					fArgs = new string[] { message.Substring(p1 + 1) };
				}
				else if(factoids[messageArray[0]].ArgStyle == "split")
				{
					fArgs = messageArray.Skip(1).ToArray();
				}
				else if(factoids[messageArray[0]].ArgStyle == "posix")
				{
					Irc.RfcPrivmsg(channel,"posix factoid style is not implemented");
				}
				
				Factoid.Exec(factoids[messageArray[0]], this, nick, channel, fArgs);
			}
			else
			{
				Irc.RfcPrivmsg(channel,"welp, no.");
			}
		}
	
		Dictionary<string,string> nickservAccounts = new Dictionary<string, string>();
		public void OnNickChange(object sender, NickChangeEventArgs e)
		{
			nickservAccounts.Remove(e.OldNickname);
		}
		
		public bool UseNickserv = true;

		public string GetNickserv(string nick)
		{
			if(!UseNickserv)
			{
				return nick;
			}

			if(nickservAccounts.ContainsKey(nick))
			{
				return nickservAccounts[nick];
			}

			string s = null;

			Irc.WriteLine("WHOIS " + nick);
			
			string resp = Irc.ReadLine(true);
			while(resp != "")
			{
				string[] split = resp.Split(new char[] { ' ' });
				if(split[1] == "318")
				{
					break;
				}	
				else if(split[1] == "330")
				{
					s = split[4];
					//break;
				}
				resp = Irc.ReadLine(true);
				
			}
			
			nickservAccounts[nick] = s;
			return s;
		}


		public IrcClient Irc { get; private set; }
		public Dictionary<string,Command> CommandList = new Dictionary<string, Command>();
				
		static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
		public static long CurrentUnixTime()
		{
			return  (long)(DateTime.UtcNow - UnixEpoch).TotalSeconds;
		}
	}

	class MainClass
	{
		public static int NumBots = 0;
		public static object NumBotsLock=new object();

		public static int Main (string[] args)
		{
			if(args.Length >= 2 && args[0] == "waitforexit")
			{
				Console.WriteLine("Waiting for exit.");
				try
				{
					Process.GetProcessById (int.Parse(args[1])).WaitForExit (2000);
				}
				catch(ArgumentException)
				{
					// C# logic!
					Console.WriteLine ("Process couldn't be found, it has exited.");
				}
			}

			Http.Init();
			List<string> configs = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText("bots.json"));
			List<Thread> threads = new List<Thread>();
			foreach(string conf in configs)
			{
				Bot bot = new Bot(conf);
				bot.GenConf(conf);
				bot.Connect();
				threads.Add(new Thread( () => ( bot.Listen() ) ));
				threads[threads.Count-1].Start();
			}

			foreach(Thread t in threads)
			{
				t.Join();
				Console.WriteLine("Thread exited!");
			}

			return 0;
		}
	}
}
