﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;

using System.Collections;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace StaryV2
{
	public static class Configuration
	{
		public static bool AutoSave = false;

		public static ConfigNode root = null;
		
		static JsonSerializerSettings settings = new JsonSerializerSettings();

		static Configuration()
		{
			settings.TypeNameHandling=TypeNameHandling.All;
			settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
			settings.StringEscapeHandling = StringEscapeHandling.EscapeNonAscii;

			Newtonsoft.Json.Serialization.DefaultContractResolver dcr = new Newtonsoft.Json.Serialization.DefaultContractResolver();
			dcr.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;
			settings.ContractResolver = dcr;
		}

		public static void Load()
		{
			if(!File.Exists("config.json"))
			{
				Console.WriteLine("No config found!");
				//Console.WriteLine("Configuring.. ");
				root = new ConfigNode("root");
				
				/*Console.Write("network name: ");
				string net = Console.ReadLine();

				Console.Write("irc.server: ");
				Set(net + ".irc.server",Console.ReadLine());
				Console.Write("irc.port: ");
				Set(net + ".irc.port",long.Parse(Console.ReadLine()));

				Console.Write("irc.ssl: ");
				Set(net + ".irc.ssl",bool.Parse(Console.ReadLine()));


				Console.Write("irc.username (enter skips): ");
				string user = Console.ReadLine();
				if(user != "")
				{
					Set(net + ".irc.username",user);
					Console.Write("irc.password: ");
					Set(net + ".irc.password",Console.ReadLine());
				}

				Console.Write("irc.nick: ");
				Set(net + ".irc.nick",Console.ReadLine());
				Console.Write("irc.nickservUser (enter skips): ");
				user = Console.ReadLine();
				if(user != "")
				{
					Set(net + ".irc.nickservUser",user);
					Console.Write("irc.nickservPass: ");
					Set(net + ".irc.nickservPass",Console.ReadLine());
				}
				Console.Write("Does the server have services? ((y)es/(n)o) ");
				string s = Console.ReadLine();
				if(s != "y" && s != "n" && s != "yes" && s != "no")
				{
					Console.WriteLine("Please enter (y)es or (n)o.");

					while(s!="y" && s != "n" && s != "yes" && s != "no")
					{
						s = Console.ReadLine();
					}
				}

				if(s == "no" || s == "n")
				{
					Set(net + ".irc.useNickserv",false);
				}
				else
				{
					Set(net + ".irc.useNickserv",true);
				}*/

				Set("factoid.data",new Dictionary<string,Factoid>());
				//Set(net + ".irc.autoJoinList",new List<string>());

				List<string> all = new List<string>();
				all.Add("*");

				Set("perms.cmd.login",all);
				Set("perms.cmd.register",all);
				Set("perms.cmd.logout",all);

				Console.WriteLine("All good!");

				Save();
				AutoSave = true;
			}
			else
			{
				root = JsonConvert.DeserializeObject<ConfigNode>(File.ReadAllText("config.json"),settings);
			}
		}

		public static void Save()
		{
			File.WriteAllText("config.json",JsonConvert.SerializeObject(root,Formatting.Indented,settings));
		}
	
		public static T GetOrCreate<T>(string location, T @default)
		{
			return (T)GetOrCreate(location, typeof(T), @default);
		}
		
		public static object GetOrCreate(string location,Type t, object @default,ConfigNode node = null)
		{
			ConfigNode n = GetNode(location,node);
			if(n == null)
			{
				Set(location,@default != null ? @default : Activator.CreateInstance(t));
				return Get(location);
			}
			else
			{
				return n.Value;
			}
		}
		
		public static T Get<T>(string location)
		{
			return (T) Get(location);
		}		

		public static object Get(string location,ConfigNode node = null)
		{
			ConfigNode n = GetNode(location,node);
			
			if(n!= null)
			{
				return n.Value;
			}
			return null;
		}
		
		public static ConfigNode GetNode(string location,ConfigNode node = null)
		{
			if(root == null)
			{
				Load();
			}

			if (node == null)
			{
				node = root;
			}

			string v, rest;
			if (location.IndexOf ('.') == -1)
			{
				try
				{
					node = node.Children.Single ((ConfigNode n) => (n.Name == location));
				}
				catch(InvalidOperationException)
				{
					return null;
				}

				return node;
			}
			else
			{
				rest = location.Substring (location.IndexOf ('.') + 1);
				v = location.Substring (0, location.IndexOf ('.'));
					
				if (node.Children.Count == 0)
				{
					return null;
				}
				try
				{
					node = node.Children.Single ((ConfigNode n) => (n.Name == v));
				}
				catch(InvalidOperationException)
				{
					return null;
				}

				return GetNode(rest, node);
			}
		}

		public static void Set(string location, object value,ConfigNode node = null)
		{
			if (node == null)
			{
				node = root;
			}

			string v, rest;
			if (location.IndexOf ('.') == -1)
			{
				try
				{
					node = node.Children.Single ((ConfigNode n) => (n.Name == location));
				}
				catch(InvalidOperationException)
				{
					ConfigNode node_ = new ConfigNode (location);
					node.Children.Add (node_);
					node=node_;
				}

				node.Value = value;

				if(AutoSave)
				{
					Save ();
				}
			} 
			else
			{
				rest = location.Substring (location.IndexOf ('.') + 1);
				v = location.Substring (0, location.IndexOf ('.'));

				if (node.Children.Count == 0)
				{
					node.Children.Add (new ConfigNode (v));
				}

				try
				{
					node = node.Children.Single ((ConfigNode n) => (n.Name == v));
				}
				catch(InvalidOperationException)
				{
					node.Children.Add (new ConfigNode(v));
					node = node.Children.Single ((ConfigNode n) => (n.Name == v));
				}

				Set (rest, value, node);
			}
		}
	}
	
	public class Tag
	{	
		public string Name;
		public object Value;
	}
	
	public class ConfigNode
	{
		public string Name;
		public List<ConfigNode> Children;
		[JsonProperty] 
		Dictionary<string,Tag> Tags;
		
		public object Value;

		public ConfigNode()
		{	
		}

		public ConfigNode(string nodeName)
		{
			this.Name = nodeName;
			this.Children = new List<ConfigNode>();
			this.Value = null;
			this.Tags = new Dictionary<string, Tag>();
		}

		public override string ToString()
		{
			return base.ToString();
		}

		public ConfigNode Clone ()
		{
			ConfigNode n = new ConfigNode();
			n.Name = Name;
			n.Value = Value;
			n.Children = new List<ConfigNode>();
			foreach(ConfigNode child in Children)
			{
				n.Children.Add (child.Clone ());
			}
			
			return n;
		}
		
				
		public void SetTag(string name, Tag tag)
		{
			if(Tags == null)
			{
				Tags = new Dictionary<string,Tag>();
			}
			Tags[name] = tag;
		}
		
			
		public List<Tag> GetTags()
		{
			List<Tag> tags = new List<Tag>();
			tags.AddRange(Tags.Values);
			return tags;
		}
		
		public Tag GetTag(string name)
		{
			if(Tags == null)
			{
				Tags = new Dictionary<string,Tag>();
			}
			
			if(Tags.ContainsKey(name))
			{
				return Tags[name];
			}
			else
			{
				return null;
			}
		}
	}
}
