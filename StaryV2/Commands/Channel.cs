using System;

namespace StaryV2
{
	public partial class Commands
	{
		public static void Join(Bot bot, string nick,string channel, string[] args)
		{
			bot.Irc.RfcPrivmsg(channel,"Joining channel" + ( args.Length!=1 ? "s" : "") + " '" + string.Join(",",args) + "'");
			bot.Irc.RfcJoin (args);
		}

		public static void Part(Bot bot, string nick,string channel, string[] args)
		{
			foreach (string s in args)
			{
				bot.Irc.RfcPrivmsg(s, "Goodbye.");
				bot.Irc.RfcPart(s);
			}
		}
	}
}

