using System;
using System.Threading;
using Meebey.SmartIrc4net;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using NLua;

namespace StaryV2
{
	public partial class Commands
	{
		static Dictionary<Bot,Dictionary<string,Dictionary<string,DateTime>>> TimedBans = new Dictionary<Bot, Dictionary<string, Dictionary<string, DateTime>>> ();
		private static AutoResetEvent timedBanTimer = new AutoResetEvent(false);

		public static void Die(Bot bot, string nick,string channel, string[] args)
		{
			foreach (string name in bot.Irc.GetChannels())
			{
				bot.Irc.RfcPrivmsg (name, "Quitting, I was killed by '" + nick + "' because '" + string.Join (" ",args) + "'" );
				Thread.Sleep(100);
			}
			bot.Irc.RfcQuit ("Quitting!");
			bot.Irc.Disconnect();

			Configuration.Save ();
			lock(MainClass.NumBotsLock)
			{
				MainClass.NumBots--;
				Console.WriteLine(MainClass.NumBots);
			}

			if(MainClass.NumBots == 0)
			{
				Console.WriteLine("Exiting..");
				Environment.Exit(0);
			}
			
			throw new Exception("We're out of here!");
		}
		
		public static void Restart(Bot bot, string nick,string channel, string[] args)
		{
			foreach (string name in bot.Irc.GetChannels())
			{
				bot.Irc.RfcPrivmsg (name, "Restarting, I was killed by '" + nick + "' because '" + string.Join (" ",args) + "'" );
				Thread.Sleep(100);
			}
			
			bot.Irc.RfcQuit("Restarting");
			bot.Irc.Disconnect();
			Configuration.Save();
			
			Console.WriteLine ("Spawning new..");
			Process.Start("StaryV2.exe","waitforexit " + Process.GetCurrentProcess ().Id);
			Console.WriteLine ("Restarting.");
			
			Thread.Sleep (1000);
			Environment.Exit (0);
		}

		public static void Kick(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 1)
			{
				if (!bot.Irc.GetChannelUser (channel, Configuration.Get<string>(bot.NetworkName + ".irc.nick")).IsOp)
				{
					bot.Irc.RfcPrivmsg (channel, "I'm not an op :(");
				} 
				else
				{
					string s = "kicked you for no reason.";
					if(args.Length >= 2)
					{
						s = string.Join (" ",args.Skip(1));					
					}
					bot.Irc.RfcKick(channel, args[0],"(" + nick + ") " + s);
				}
			}
			else
			{
				throw new CommandException("Usage: <user>");
			}
		}

		public static void Ban(Bot bot, string nick,string channel, string[] args)
		{
			if (!bot.Irc.GetChannelUser (channel, Configuration.Get<string>(bot.NetworkName + ".irc.nick")).IsOp)
			{
				bot.Irc.RfcPrivmsg (channel, "I'm not an op :(");
			} 
			else
			{
				if(args.Length >= 1)
				{
					IrcUser user = bot.Irc.GetIrcUser(args[0]);
					if(user == null)
					{
						throw new CommandException("I do not know '" + args[0] + "'.");
					}
					
					string mask = "*!" + user.Ident + "@" + user.Host;
					bot.Irc.RfcMode(channel, "+b " + mask);

					if(args.Length >= 2)
					{
						int h,m,s;
						h=m=s=0;
						Match match = Regex.Match(args[args.Length-1], @"(\d?\d?)h?(\d?\d?)m?(\d?\d?)s?");
						try
						{
							foreach(Capture c in match.Captures)
							{
								if(c.Value=="")
								{
									continue;
								}
								if(c.Value.Contains ("h"))
								{
									h=int.Parse (c.Value.Substring(0,c.Value.Length-1));
								}
								else if(c.Value.Contains ("m"))
								{
									m=int.Parse (c.Value.Substring(0,c.Value.Length-1));
								}
								else if(c.Value.Contains ("s"))
								{
									s=int.Parse(c.Value.Substring(0,c.Value.Length-1));
								}
							}
						}
						catch(FormatException)
						{
							throw new CommandException("Number is invalid!");
						}

						if(h!=0 || m!=0 || s!=0)
						{
							if(!TimedBans.ContainsKey (bot))
							{
								TimedBans[bot] = new Dictionary<string, Dictionary<string, DateTime>>();
							}
							if(!TimedBans[bot].ContainsKey (channel))
							{
								TimedBans[bot][channel] = new Dictionary<string, DateTime>();
							}

							TimedBans[bot][channel][mask] = DateTime.UtcNow + new TimeSpan(h,m,s);

							timedBanTimer.Set ();
						}
					}
				}
				else
				{
					throw new CommandException("Usage: <user>");
				}
			}
		}
		
		public static void KickBan(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length > 2)
			{
				List<string> args2 = new List<string> ();
				args2.AddRange (args);
				if (Regex.IsMatch (args[args.Length-1], @"(\d?\d?)h?(\d?\d?)m?(\d?\d?)s?"))
				{
					args2.RemoveAt (args2.Count-1);
				}
	
				Kick(bot, nick, channel, args2.ToArray ());
				Ban (bot, nick, channel, args);
			}	
			else
			{
				throw new CommandException("Usage: <list,remove,add> [#channel]");
			}
		}

		public static void Autojoin(Bot bot, string nick,string channel, string[] args)
		{
			if(Configuration.Get(bot.NetworkName + ".irc.autoJoinList") == null)
			{
				Configuration.Set(bot.NetworkName + ".irc.autoJoinList", new List<string>());
			}

			if(args.Length != 0 && args[0] == "list")
			{
				bot.Irc.RfcNotice(nick,string.Join (",",Configuration.Get<List<string>>(bot.NetworkName + ".irc.autoJoinList")));
			}
			else if(args.Length >= 2)
			{
				if(args[0] == "remove")
				{
					List<string> autojoin = Configuration.Get<List<string>>(bot.NetworkName + ".irc.autoJoinList");
					autojoin.Remove(args[1]);
					Configuration.Set (bot.NetworkName + ".irc.autoJoinList",autojoin);
				}
				else if(args[0] == "add")
				{
					List<string> autojoin = Configuration.Get<List<string>>(bot.NetworkName + ".irc.autoJoinList");
					autojoin.Add (args[1]);
					Configuration.Set (bot.NetworkName + ".irc.autoJoinList",autojoin);
				}
			}
			else
			{
				throw new CommandException("Usage: <list,remove,add> [#channel]");
			}
		}

		public static void Load(Bot bot, string nick,string channel, string[] args)
		{
			Configuration.Load();
			bot.Irc.RfcPrivmsg (channel, "Config loaded!");
		}
		
		public static void Save(Bot bot, string nick,string channel, string[] args)
		{
			Configuration.Save();
			bot.Irc.RfcPrivmsg (channel, "Config saved!");
		}
		
		public static void TimerThread()
		{
			while (true)
			{
				int earliest = int.MaxValue;
				string earliest_mask = null;
				string earliest_channel = null;
				Bot earliest_bot = null;

				foreach(KeyValuePair<Bot,Dictionary<string,Dictionary<string,DateTime>>> kv in TimedBans)
				{
					foreach (KeyValuePair<string,Dictionary<string,DateTime>> kv2 in kv.Value)
					{
						foreach (KeyValuePair<string,DateTime> kv3 in kv2.Value)
						{
							TimeSpan diff = (kv3.Value - DateTime.UtcNow);
							if (diff.TotalMilliseconds < earliest)
							{
								earliest = (int)diff.TotalMilliseconds;
								earliest_channel = kv2.Key;
								earliest_mask = kv3.Key;
								earliest_bot = kv.Key;
							}
						}
					}
				}
					  
				if (earliest < 0)
				{
					earliest_bot.Irc.RfcMode (earliest_channel,"-b " + earliest_mask);
				} 
				else
				{
					bool interrupted = timedBanTimer.WaitOne (earliest);
					if(!interrupted && earliest_mask!=null)
					{
						earliest_bot.Irc.RfcMode (earliest_channel,"-b " + earliest_mask);
						TimedBans[earliest_bot][earliest_channel].Remove (earliest_mask);
					}
				}
			}
		}
		
		public static void Add(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				object o = Configuration.Get (args[0]);
				if(o.ToString () == "System.Collections.Generic.List`1[System.String]")
				{
					List<string> list = (List<string>)o;
					string s = string.Join (" ", args.Skip(1));
					list.Add(s);		
					Configuration.Set(args[0],list);
				}
			}
			else
			{
				throw new CommandException("Usage: <node> <value>");
			}
		}
		
		public static void Remove(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				object o = Configuration.Get (args[0]);
				if(o.ToString () == "System.Collections.Generic.List`1[System.String]")
				{
					List<string> list = (List<string>)o;
					list.RemoveAt (int.Parse (args[1]));
					Configuration.Set (args[0],list);
				}
			}
			else
			{
				throw new CommandException("Usage: <node> <position>");
			}
		}
		
		public static void Set(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				ConfigNode n = Configuration.GetNode(args[0]);
				if(n != null)
				{
					if(n.GetTag("perms") != null)
					{
						List<string> perms = (List<string>)n.GetTag("perms").Value;
						string user = User.GetOnNetwork(bot, nick, bot.NetworkName).Username;
						bool ok = false;
						foreach(string perm in perms)
						{
							if(user == perm)
							{
								ok = true;
								break;
							}
						}
						if(!ok)
						{
							bot.Irc.RfcPrivmsg(channel,"...gtfo please.");
							return;
						}
					}
				}

				string str = string.Join (" ", args.Skip(1));	  
				
				Configuration.Set (args[0],ExtendedArgs.Parse(str));
			}
			else
			{
				throw new CommandException("Usage: <node> <value>");
			}
		}
		
		public static void Get(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length !=0)
			{
				ConfigNode n = Configuration.GetNode(args[0]);
				if(n!=null)
				{
					if(n.GetTag("perms") != null)
					{
						List<string> perms = (List<string>)n.GetTag("perms").Value;
						string user = User.GetOnNetwork(bot, nick, bot.NetworkName).Username;
						bool ok = false;
						foreach(string perm in perms)
						{
							if(user == perm)
							{
								ok = true;
								break;
							}
						}
						if(!ok)
						{
							bot.Irc.RfcPrivmsg(channel,"...gtfo please.");
							return;
						}
					}
					
					object obj = n.Value;
					string s = obj.ToString ();
					
					bot.Irc.RfcPrivmsg (channel, s);
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel,"Node does not exist.");
				}			
			}
			else
			{
				throw new CommandException("Usage: <node>");
			}
		}
		
		public static void Children(Bot bot, string nick,string channel, string[] args)
		{
			ConfigNode n = null;

			if(args.Length != 0)
			{
				string s=string.Empty;
				bool any = false;
				n = Configuration.GetNode(args[0]);
				if(n==null)
				{
					bot.Irc.RfcPrivmsg(channel, "There is no node at " + args[0]);
					return;
				}
				
				foreach(ConfigNode child in n.Children)
				{
					s += child.Name + ", " ;
					any = true;
				}
				if(any)
				{
					s = s.Substring (0,s.Length - 2);
				}
				else
				{
					s = "No children.";
				}
				bot.Irc.RfcPrivmsg(channel, s);
			}
			else
			{
				n = Configuration.root;

				string s=string.Empty;
				foreach(ConfigNode child in n.Children)
				{
					s += child.Name + ", " ;
				}
				s = s.Substring (0,s.Length - 2);
				bot.Irc.RfcPrivmsg(channel, s);
			}
		}
		
		public static void GetTags(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length !=0)
			{
				ConfigNode n = Configuration.GetNode(args[0]);
				if(n!=null)
				{
					foreach(Tag tag in n.GetTags())
					{
						string s = ExtendedArgs.Serialize(tag.Value);
						
						bot.Irc.RfcPrivmsg(channel, tag.Name + "=" + s);
					}
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel,"Node does not exist!");
				}
			}
			else
			{
				throw new CommandException("Usage: <node>");
			}
		}
		
		public static void SetTag(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 3)
			{
				ConfigNode n = Configuration.GetNode(args[0]);
				if(n!=null)
				{
					Tag tag = new Tag();
					tag.Name = args[1];
					
					string s = string.Concat(args.Skip(2));

					tag.Value = ExtendedArgs.Parse(s);
					
					n.SetTag(args[1], tag);
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel,"Node does not exist!");
				}
			}
			else
			{
				throw new CommandException("Usage: <node> <tag> <value>");
			}
		}
		
		
		public static void Auth(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				if(bot.CommandList.ContainsKey(args[0]))
				{
					List<string> perms = Configuration.Get<List<string>>("perms.cmd." + args[0]);
					
					string user = User.GetOnNetwork(bot, nick, args[1]).Username;
					
					if(user!=null)
					{
						perms.Add(user);
						Configuration.Set("perms.cmd." + args[0], perms);
					}
					else
					{
						bot.Irc.RfcPrivmsg(channel,args[1] + " is not identified!");
					}
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel, "There is no command called '"+args[0]+"'");
				}
			}
			else
			{
				throw new CommandException("Usage: <command> <user>");
			}
		}
		
		public static void LAuth(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				if(bot.CommandList.ContainsKey(args[0]))
				{
					List<string> perms = Configuration.GetOrCreate<List<string>>(bot.NetworkName + ".channel." + channel + ".perms.cmd." + args[0],null);
					
					string user = User.GetOnNetwork(bot, args[1], bot.NetworkName).Username;
					
					if(user!=null)
					{
						perms.Add(user);
						Configuration.Set(bot.NetworkName + ".channel." + channel + ".perms.cmd." + args[0], perms);
					}
					else
					{
						bot.Irc.RfcPrivmsg(channel,args[1] + " is not identified!");
					}
				}
				else
				{
					bot.Irc.RfcPrivmsg(channel, "There is no command called '"+args[0]+"'");
				}
			}
			else
			{
				throw new CommandException("Usage: <command> <user>");
			}
		}
		
		public static void Clones(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length == 0) { throw new CommandException("Usage: <glob>"); }
			Regex regex = new Regex(args[0].Replace(".","\\.").Replace("\\*","\xFF").Replace("*",".*").Replace("\xFF","*"));
			Dictionary<string,string> hosts = new Dictionary<string, string>();
			foreach(string chan in bot.Irc.GetChannels())
			{
				foreach(object o in bot.Irc.GetChannel(chan).Users.Values)
				{
					NonRfcChannelUser u = ((NonRfcChannelUser)o);
					hosts[u.Nick  + " in " + chan] = u.Host;
				}
			}
			
			List<string> clones = new List<string>();
			
			foreach(KeyValuePair<string,string> kv in hosts)
			{
				if(regex.IsMatch(kv.Value))
				{
					clones.Add(kv.Key + " | " + kv.Value);
				}
			}
			bot.Irc.RfcPrivmsg(channel,"found " + clones.Count + " clones.");
			bot.Irc.RfcNotice(nick, string.Join(", ", clones.ToArray()));
		}

		public static void Spawn(Bot bot, string nick,string channel, string[] args)
		{
			Console.WriteLine("Spawning clone..");
			Bot b = new Bot(bot.NetworkName);
			b.Connect();
			Console.WriteLine("Listening clone..");
			Thread t = new Thread(() => { b.Listen(); } );
			t.Start();
		}
	}
}

