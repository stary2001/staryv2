using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace StaryV2
{
	public partial class Commands
	{
		public static void FLock(Bot bot, string nick,string channel, string[] args)
		{
			throw new NotImplementedException();
		}
		
		public static void FInfo(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length <= 1) { return; }
			
			Dictionary<string,Factoid> factoids = Configuration.Get<Dictionary<string,Factoid>>("factoid.data");
			
			if(!factoids.ContainsKey(args[0]))
			{
					bot.Irc.RfcPrivmsg(channel, "Factoid '"+args[0] + "' does not exist!");
					return;
			}
			Factoid target = factoids[args[0]];
				
			if(args.Length == 2)
			{
				if(args[1] == "lang")
				{
					bot.Irc.RfcPrivmsg(channel,target.Lang);
				}
				else if(args[1] == "argStyle")
				{
					bot.Irc.RfcPrivmsg(channel,target.ArgStyle);
				}
				else if(args[1] == "lastMod")
				{
					bot.Irc.RfcPrivmsg(channel,target.LastMod);
				}
				else if(args[1] == "lastModTime")
				{
					long ago = (Bot.CurrentUnixTime() - target.LastModTime);
					
					bot.Irc.RfcPrivmsg(channel,string.Format("{0:00}:{1:00}:{2:00} ago",ago/3600,(ago/60)%60,ago%60));
				}
			}
			
			if(args.Length >= 3)
			{
				if(args[1] == "set")
				{
					if(args[2] == "lang")
					{
						target.Lang = args[3];
						Configuration.Set("factoid.data",factoids);
					}
					else if(args[2] == "argStyle")
					{
						target.ArgStyle = args[3];
					}
				}
				else if(args[1] == "get")
				{
					if(args[2] == "lang")
					{
						bot.Irc.RfcPrivmsg(channel,target.Lang);
					}
					else if(args[2] == "argStyle")
					{
						bot.Irc.RfcPrivmsg(channel,target.ArgStyle);
					}
				}
			}
		}
		
		public static void Remember(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 2)
			{
				Factoid.Add(bot, args, nick);
			}
		}

		public static void Forget(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length >= 1)
			{
				Factoid.Remove(bot, args[0]);
			}
		}
	}
	
	[JsonObject(MemberSerialization.OptIn)]
	public class Factoid
	{
		public delegate void FactoidHandler(Factoid f, Bot bot,string nick, string channel, string[] args);
		
		static Dictionary<string,FactoidHandler> Handlers=new Dictionary<string, FactoidHandler>();
		[JsonProperty]
		public string Source {get; set;}
		[JsonProperty]
		public string LastMod {get; set;}
		[JsonProperty]
		public long LastModTime {get; set;}
		[JsonProperty]
		public string Lang {get; set;}
		[JsonProperty]
		public string ArgStyle {get; set;}
		
		static Factoid()
		{
			Handlers["plain"] = new FactoidHandler( (Factoid f, Bot bot, string nick, string channel, string[] args) => { bot.Irc.RfcPrivmsg(channel, f.Source); } );
			Handlers["lua"] = new FactoidHandler( (Factoid f, Bot bot, string nick, string channel, string[] args) => 
			{ 
				Lua.Run(bot,nick,channel,f.Source,args);
			});
		}
		
		public Factoid(string src,string lastMod = "", long lastModTime = 0,string lang="plain", string argStyle = "split")
		{
			Source = src;
			Lang = lang;
			ArgStyle = argStyle;
			LastMod = lastMod;
			LastModTime = lastModTime;
		}
		
		public static void Add(Bot bot, string[] args, string nick)
		{
			var factoids = Configuration.Get<Dictionary<string,Factoid>>("factoid.data");
			string lang = "plain";
			string argStyle = "split";
			if(factoids.ContainsKey(args[0]))
			{
				lang = factoids[args[0]].Lang;
				argStyle = factoids[args[0]].ArgStyle;
			}
			factoids[args[0]] = new Factoid(string.Join(" ",args.Skip(1)), bot.GetNickserv(nick), Bot.CurrentUnixTime(),lang,argStyle);
			Configuration.Set ("factoid.data", factoids);
		}
		
		public static void Remove(Bot bot,string factoid)
		{
			var factoids = Configuration.Get<Dictionary<string,Factoid>>("factoid.data");
			factoids.Remove(factoid);
			Configuration.Set ("factoid.data", factoids);
		}
		
		public static void Exec(Factoid f, Bot bot,string nick, string channel, string[] args)
		{
			Handlers[f.Lang](f,bot,nick,channel,args);
		}
		
		public override string ToString()
		{
			return "Source = '" + Source + "'";
		}
	}
}
