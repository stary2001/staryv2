using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Reflection;
using Meebey.SmartIrc4net;
using System.Collections;

namespace StaryV2
{
	public static class LuaExtensions
	{
		public static NLua.LuaTable NewTable(this NLua.Lua lua)
		{
			lua.NewTable("ifYouUseThisNameInLuaYouAreStupid");
			NLua.LuaTable t = (NLua.LuaTable)lua["ifYouUseThisNameInLuaYouAreStupid"];
			lua["ifYouUseThisNameInLuaYouAreStupid"] = null;
			return t;
		}
	}
	
	public class LuaBot
	{
		public LuaBot() {}
		public Bot Bot;
		public string Nick;
		public string Channel;
		public string Host;
	}
	
	public static class Lua
	{
		static string ErrMsg = string.Empty;

		public static NLua.Lua LuaState;
		
		static NLua.LuaFunction LuaMsg = null;

		static bool ShouldStop;
		
		public static object[] Get(LuaBot b , string path)
		{
			object o = Configuration.Get("lua." + path);
			return new object[] { o };
		}
		
		public static object[] Set(LuaBot b , string path, object o)
		{
			//object[] o = LuaState.GetFunction ("unpack").Call (t);
			Configuration.Set("lua." + path, o);
			return new object[0];
		}
		
		public static object[] Write(string s)
		{
			Console.WriteLine (s);
			return new object[0];
		}
		
		public static object[] Msg(LuaBot b , NLua.LuaTable t)
		{
			try
			{
			
			Thread.Sleep(100);
			lock(typeof(Commands)) { if(ShouldStop) { Thread.CurrentThread.Abort();  } }
			if(t.Keys.Count != 0)
			{
				object[] o = LuaState.GetFunction ("unpack").Call(new object[] { t });
				if(o.Length == 0)
				{
					b.Bot.Irc.RfcPrivmsg(b.Channel,"nil");
				}
				else
				{
					b.Bot.Irc.RfcPrivmsg (b.Channel, string.Join("	", o));
				}
			}
			else
			{
				b.Bot.Irc.RfcPrivmsg(b.Channel,"nil");
			}
			return new object[0];
			
			}
			catch(ThreadAbortException e)
			{
				throw e;
			}
			catch(Exception e)
			{
				Console.WriteLine(e.ToString());
				return null;
			}
		}
		
		public static object[] ListUsers(LuaBot b)
		{
			NLua.LuaTable table = LuaState.NewTable();
			
			Hashtable u = b.Bot.Irc.GetChannel(b.Channel).Users;
			Hashtable v = b.Bot.Irc.GetChannel(b.Channel).Voices;
			Hashtable o = b.Bot.Irc.GetChannel(b.Channel).Ops;
			
			List<string> users = new List<string>();
			
			foreach(string usr in u.Keys)
			{
				if(!users.Contains(usr)) users.Add(usr);
			}
			
			foreach(string usr in v.Keys)
			{
				if(!users.Contains(usr)) users.Add(usr);
			}
			
			foreach(string usr in o.Keys)
			{
				if(!users.Contains(usr)) users.Add(usr);
			}
			
			int i = 0;
			foreach(string usr in users)
			{
				table[i++] = usr;
			}
			
			return new object[] { table };
		}
		
		public static object[] GetUser(LuaBot b , string user)
		{
			IrcUser u = b.Bot.Irc.GetIrcUser(user);
			string ns = b.Bot.GetNickserv(user);
			NLua.LuaTable t = LuaState.NewTable();
			t["nick"] = user;
			t["user"] = ns;
			t["ident"] = u.Ident;
			t["host"] = u.Host;
			
			t["mask"] = user + "!" + u.Ident + "@" + u.Host;
			NLua.LuaTable lastMessages = LuaState.NewTable();			
			NLua.LuaTable joinedChannels = LuaState.NewTable();
			
			int i = 0;
			if(b.Bot.lastMessages.ContainsKey(ns))
			{
				foreach(string s in b.Bot.lastMessages[ns])
				{
					lastMessages[i++] = s;
				}
			}
			
			i = 0;
			foreach(string s in u.JoinedChannels)
			{
				joinedChannels[i++] = s;
			}
			t["lastMessages"] = lastMessages;
			t["channels"] = joinedChannels;
			return new object[] { t }; 
		}
		
		public static object[] RunCmd(LuaBot b, NLua.LuaTable tArgs)
		{
			try
			{
				List<string> args = new List<string>();
				foreach(KeyValuePair<object,object> kv in tArgs)
				{
					args.Add(kv.Value.ToString());
				}

				string total = string.Join(" ", args);

				b.Bot.OnCommand(b.Nick,b.Host, b.Channel, total, total.Split(' '));
			}
			catch(Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return new object[0]; 
		}
		
		public static void SetupLua()
		{
			LuaState = new NLua.Lua();
			ShouldStop = false;
			LuaState.NewTable("New_G");
			NLua.LuaTable new_G = LuaState.GetTable ("New_G");
			
			LuaState.NewTable("New_G.sharpBot");
			LuaState.NewTable("New_G.sharpBot.cfg");
			LuaState.NewTable("New_G.sharpBot.user");

			LuaState.NewTable("New_G.bot");
			LuaState.NewTable("New_G.bot.cfg");
			LuaState.NewTable("New_G.bot.user");

			LuaMsg = LuaState.RegisterFunction("New_G.sharpBot.msg",typeof(StaryV2.Lua).GetMethod("Msg"));
			LuaState.RegisterFunction("New_G.sharpBot.cfg.get",typeof(StaryV2.Lua).GetMethod("Get"));
			LuaState.RegisterFunction("New_G.sharpBot.cfg.set",typeof(StaryV2.Lua).GetMethod("Set"));
			LuaState.RegisterFunction("New_G.sharpBot.user.get",typeof(StaryV2.Lua).GetMethod("GetUser"));
			LuaState.RegisterFunction("New_G.sharpBot.user.list",typeof(StaryV2.Lua).GetMethod("ListUsers"));
			LuaState.RegisterFunction("New_G.sharpBot.runCmd",typeof(StaryV2.Lua).GetMethod("RunCmd"));
			
			LuaState["New_G.print"] = LuaMsg;

			List<string> good = new List<string>();
			good.Add ("tostring");
			good.Add ("tonumber");
			good.Add ("math");
			good.Add ("table");
			good.Add ("print");
			good.Add ("pcall");
			good.Add ("xpcall");
			good.Add ("unpack");
			good.Add ("assert");
			good.Add ("setmetatable");
			good.Add ("getmetatable");
			good.Add ("next");
			good.Add ("pairs");
			good.Add ("ipairs");
			good.Add ("loadstring");
			good.Add ("_VERSION");
			good.Add ("New_G");
			good.Add ("type");
			good.Add ("select");
			good.Add ("rawset");
			good.Add ("rawget");
			good.Add ("setfenv");
			good.Add ("error");
			good.Add ("string");
			good.Add ("rawequal");
			good.Add ("collectgarbage");
			good.Add ("gcinfo");
			// no getfenv, or everything will come crashing down
			
			foreach(KeyValuePair<object,object> kv in LuaState.GetTable ("_G"))
			{
				if(good.Contains((string)kv.Key))
				{
					new_G[kv.Key]=kv.Value;
				}
				else
				{
					Console.WriteLine ("Removed " + kv.Key);
				}
			}
			
			NLua.LuaFunction f = LuaState.LoadFile("init.lua");
			LuaState.GetFunction("setfenv").Call (f,new_G);
			
			//Full_G = LuaState.GetTable ("_G");
			LuaState["_G"] = LuaState["New_G"];
			LuaState["New_G._G"] = LuaState["New_G"];

			f.Call ();
		}
		
		public static bool Setup = false;
		
		public static void Executor(object o)
		{
			object[] arr = (object[])o;
			Bot bot = (Bot)arr[0];
			string nick = (string)arr[1];
			string channel = (string)arr[2];
			string source = (string) arr[3];
			
			object[] args = null;
			if(arr.Length == 5)
			{
				args=(object[]) arr[4];	
			}
			
			NLua.LuaTable new_G = null;
			
			if(!Setup) { SetupLua(); Setup = true; }
			
			new_G = LuaState.GetTable ("New_G");
			
			NLua.LuaFunction f2 = null;
			try
			{
				f2 = LuaState.LoadString(source,nick);
			}
			catch(Exception e)
			{
				ErrMsg = e.Message;
				Thread.CurrentThread.Abort();
			}
			
			LuaBot b = new LuaBot();
			b.Bot = bot;
			b.Nick = nick;
			b.Host = bot.Irc.GetIrcUser(nick).Host;

			b.Channel = channel;

			new_G["sharpBot.bot"] = b;
			new_G["bot.user.current"] = nick;

			bool sandbox = true;
			
			string ns = bot.GetNickserv(nick);
			List<string> perms = Configuration.GetOrCreate<List<string>>("perms.lua.g",null);
			foreach (string s in perms)
			{
				if (s == ns)
				{
					sandbox = false;
				}
			}

			if(sandbox) LuaState.GetFunction("setfenv").Call (f2,new_G);
			
			
			Thread.Sleep(100);

			try
			{
				if(args != null)
				{
					f2.Call(args);
				}
				else
				{
					f2.Call ();
				}
			}
			catch(Exception e)
			{
				ErrMsg = e.Message;
				Thread.CurrentThread.Abort();
			}
		}	
		
		public static void Run(Bot bot, string nick,string channel, string source, object[] args)
		{
			Thread executorThread = new Thread(Executor);
			executorThread.Start(new object[] {bot,nick,channel,source,args});
			executorThread.Join(500);
			if(executorThread.IsAlive)
			{
				lock(typeof(Commands)) { ShouldStop = true; }
				executorThread.Abort();
				executorThread.Join();
				lock(typeof(Commands)) { ShouldStop = false; }
				bot.Irc.RfcPrivmsg(channel, "Lua thread was aborted after 500ms.");
			}
			else
			{
				if(ErrMsg != null)
				{
					bot.Irc.RfcPrivmsg(channel, ErrMsg);
					ErrMsg = null;
				}
			}
		}
	}
	
	public partial class Commands
	{
		public static void Lua(Bot bot, string nick,string channel, string[] args)
		{
			StaryV2.Lua.Run(bot,nick,channel,string.Join(" ",args),null);
		}
	}
}

