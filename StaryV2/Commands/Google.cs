using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace StaryV2
{
	public partial class Commands
	{

		public static string PostAPIEndpoint(string endpoint, string postData)
		{
			string GoogleApiKey = Configuration.Get<string>("google.key");
			WebRequest req = HttpWebRequest.Create ("https://www.googleapis.com/" + endpoint + "?key=" + GoogleApiKey);
			req.ContentType = "application/json";
			req.Method = "POST";

			StreamWriter w = new StreamWriter(req.GetRequestStream());
			w.Write (postData);
			w.Flush ();

			WebResponse resp = req.GetResponse ();
			w.Close ();
			return new StreamReader (resp.GetResponseStream ()).ReadToEnd();
		}

		public static string GetAPIEndpoint(string endpoint, string args)
		{
			string GoogleApiKey = Configuration.Get<string>("google.key");
			WebRequest req = HttpWebRequest.Create ("https://www.googleapis.com/" + endpoint + "?key=" + GoogleApiKey + args);
			WebResponse resp = req.GetResponse ();

			return new StreamReader (resp.GetResponseStream ()).ReadToEnd();
		}

		public static void Google(Bot bot, string nick,string channel, string[] args)
		{
			if (args.Length != 0)
			{
				string searchid = Configuration.Get<string>("google.customsearchid");
				string jsonData = GetAPIEndpoint ("customsearch/v1", "&cx=" + searchid +"&q=" + string.Join ("%20", args));

				JObject root = JObject.Load (new JsonTextReader (new StringReader (jsonData)));
				JArray items = (JArray)root ["items"];
				bot.Irc.RfcPrivmsg (channel, nick + ": " + items.First["link"] + " -- " + items.First ["title"].ToString () + ": " + items.First["snippet"].ToString ());
			}
		}

		public static void Youtube(Bot bot, string nick,string channel, string[] args)
		{
			if (args.Length != 0)
			{
				string jsonData = GetAPIEndpoint ("youtube/v3/search", "&part=snippet&q=" + string.Join ("%20", args));

				JObject root = JObject.Load (new JsonTextReader (new StringReader (jsonData)));
				JArray items = (JArray)root["items"];
				JObject snippet = (JObject)items.First["snippet"];
				string videoData = GetAPIEndpoint("youtube/v3/videos", "&part=contentDetails,statistics&id=" + items.First["id"]["videoId"].ToObject(typeof(string)));

				JObject videoRoot = JObject.Load (new JsonTextReader (new StringReader (videoData)));
				JArray videoItems = (JArray)videoRoot["items"];


				double likes = double.Parse (videoItems.First["statistics"]["likeCount"].ToString ());
				double dislikes = double.Parse (videoItems.First["statistics"]["dislikeCount"].ToString ());

				double rating = (likes * 5 + dislikes) / (likes + dislikes);

				string msg = "\x02" + snippet["title"].ToString () +
				"\x0f | length \x02" + videoItems.First["contentDetails"]["duration"].ToString ().Substring (2).ToLower () +
				"\x0f | rated \x02" + Math.Round (rating,2) + "/5.00" +
				"\x0f | \x02" + videoItems.First["statistics"]["viewCount"] + "\x0f views" + 
				" | by \x02" + snippet["channelTitle"].ToString () + 
				"\x0f | http://youtu.be/" + items.First["id"]["videoId"];

				bot.Irc.RfcPrivmsg (channel,msg);
			}
		}

		public static void Googl(Bot bot, string nick,string channel, string[] args)
		{
			if(args.Length != 0)
			{
				string jsonData = PostAPIEndpoint("urlshortener/v1/url","{\"longUrl\": \"" + string.Join ("%20", args) + "\"}");
				JObject root = JObject.Load (new JsonTextReader (new StringReader (jsonData)));
				bot.Irc.RfcPrivmsg(channel,root["id"].ToString ());
			}
		}
	}
}

