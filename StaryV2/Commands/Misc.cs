using System;
using System.Collections.Generic;

namespace StaryV2
{
	public partial class Commands
	{	
		public static void List(Bot bot, string nick,string channel, string[] args)
		{
			bot.Irc.RfcNotice(nick,"Commands: " + string.Join(", ", bot.CommandList.Keys));
			string s = "Commands you can use: ";

			string nickserv = bot.GetNickserv(nick);

			bool anyButList = false;
			
			foreach(string cmd in bot.CommandList.Keys)
			{
				List<string> perms = Configuration.Get<List<string>>("perms.cmd."+cmd);
				if(perms != null && perms.Contains(nickserv))
				{
					if(cmd != "list") anyButList = true;
					s += cmd + ", ";
				} 
			}

			s = s.Substring (0, s.Length - 2);
			
			if(!anyButList)
			{
				s=s.Substring(0,s.Length - 4); // trim list
				s+="none. Loner. :(";
			}
			
			bot.Irc.RfcNotice (nick, s);
		}
	}
}

