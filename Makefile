CSHARPFILES=$(wildcard StaryV2/*.cs) $(wildcard StaryV2/*/*.cs)

all: StaryV2

bin/Newtonsoft.Json.dll:
	mcs -recurse:Newtonsoft.Json/Src/Newtonsoft.Json/*.cs -r:System.Numerics -r:System.Xml.Linq -r:System.Runtime.Serialization -r:System.Data -t:library -d:NET35 -out:bin/Newtonsoft.Json.dll

bin/SmartIrc4net.dll:
	mcs -recurse:SmartIrc4net/src/*.cs -out:bin/SmartIrc4net.dll -t:library

bin/NLua.dll:
	sh -c 'cd NLua; xbuild /p:Configuration=ReleaseKopiLua NLua.sln'
	cp NLua/Core/NLua/bin/ReleaseKopiLua/NLua.dll bin
	cp NLua/Core/NLua/bin/ReleaseKopiLua/KopiLua.dll bin

bin/init.lua:
	ln -s init.lua bin/init.lua

user=bot
host=stary2001.co.uk
dir=~

screen=StaryV2

deploy: 
	ssh $(user)@$(host) rm $(dir)/StaryV2.exe -f;
	scp bin/StaryV2.exe $(user)@$(host):$(dir);
	-ssh $(user)@$(host) "ps ax | grep '[m]ono StaryV2.exe' | cut -d' ' -f1 | xargs kill";
	sleep 1;
	ssh $(user)@$(host) "nohup mono StaryV2.exe > $(dir)/log &"

force-deploy-all:
	ssh $(user)@$(host) rm $(dir)/* -r;
	scp -r bin/StaryV2.exe $(user)@$(host):$(dir)

StaryV2: $(CSHARPFILES) bin/Newtonsoft.Json.dll bin/SmartIrc4net.dll bin/NLua.dll
	 mcs -recurse:StaryV2/*.cs -r:bin/SmartIrc4net.dll -r:bin/Newtonsoft.Json.dll -r:bin/NLua.dll -out:bin/StaryV2.exe -debug+

clean:
	rm bin -r;
	mkdir bin
