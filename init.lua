print("init.lua called!")

function bot.msg(...)
	sharpBot.msg(sharpBot.bot,{...})
end

function bot.cfg.get(path)
	return sharpBot.cfg.get(sharpBot.bot,path)[0]
end

function bot.cfg.set(path,obj)
	sharpBot.cfg.set(sharpBot.bot,path,obj)
end

function bot.user.get(nick)
	return sharpBot.user.get(sharpBot.bot,nick)[0]
end

function bot.user.list()
	return sharpBot.user.list()[0]
end

function bot.runCmd(...)
	return sharpBot.runCmd(sharpBot.bot,{...})
end
