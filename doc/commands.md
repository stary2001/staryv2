#Command reference
### this is not a complete reference and probably never will be

### Admin

* die [reason]

    Kills the bot with an optional reason.
    
    perms.cmd.die

* restart [reason]

    Restarts the bot, again with an optional reason.
    
    perms.cmd.restart
    
* kick <user> <reason>  

    Kicks specified user from current channel.
    
    perms.cmd.kick
    
* ban <user> [time]

    Bans specified user from channel, with an optional time.
    
    perms.cmd.ban
    
* kickban <user> <reason> [time]

    Kicks and bans specified user from current channel, with an optional time.
    
    perms.cmd.kickban
    
* autojoin <list, remove, add> [#channel]

    If #channel is included, operates on that channel. Otherwise, operates on current channel.
    
    perms.cmd.autojoin
    
* auth <command> <user>

    Authenticates an account to use the command specified.
    
    perms.cmd.auth
    
* lauth <command> <user>

    Authenticates an account to use the command specified _on the current network_.
    
    perms.cmd.lauth
    
* clones <glob>

    Gets all users the bot can see whose hostmask matches the glob.
    
    perms.cmd.clones
    
* spawn <network>

    Spawns a new bot with the specified network name.
    
    perms.cmd.spawn

### Configuration

    
* load

    Loads configuration from disk.
    
    perms.cmd.load
    
* save

    Saves configuration to disk.
    
    perms.cmd.save
    
* add <node> <value>

    Adds an entry to a list in the configuration system.
    
    perms.cmd.add

* set <node> <value>

    Sets the value of a node in the configuration system.
    
    perms.cmd.set
    
* get <node>

    Gets the value of a node in the configuration system.
    perms.cmd.get
    
* children <node>

    Gets the children of a node in the configuration system.
    
    perms.cmd.children
    
* gettags <node>

    Gets the tags of a node in the configuration system.
    
    perms.cmd.gettags
    
* settag <node> <tag> <value>

    Gets a tag of a node in the configuration system.
    
    perms.cmd.settag